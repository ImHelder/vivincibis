import React from 'react';
import { db } from '../../firebaseConfig'
import './PriseRDV.css'

import { collection, addDoc, getDocs } from 'firebase/firestore'
import times from "./time.json";

function PriseRDV() {

    const [available, setAvailable] = React.useState([]);
    const [speciality, setSpeciality] = React.useState('');

    React.useEffect(() => {
        (async () => {
            const querySnapshot = await getDocs(collection(db, 'users'));
            const values = querySnapshot.docs.map(doc => ({ ...doc.data(), id: doc.id }));

            console.log(speciality);
            const newAvailable = values.filter(user => user.spécialité.includes(speciality));

            console.log(newAvailable);
            setAvailable(newAvailable);
        })();
    }, [speciality]);


    const changeSpeciality = (event) => {
        const value = event.target.value;
        setSpeciality(value);
    }



    return (
        <div className="PriseRDV">
            <button className="PriseRDV-jesuismedecin">Je suis médecin</button>
            <div className="PriseRDV-infos">
                <input className="input" type="text" placeholder="Nom" />
                <input className="input" type="text" placeholder="Prénom" />
                <select className='input' onChange={changeSpeciality}>
                    <option defaultValue disabled selected>Sélectionnez une spécialité</option>
                    <option value="Cardiologue">Cardiologue</option>
                    <option value="Généraliste">Généraliste</option>
                    <option value="Pédiatre">Pédiatre</option>
                    <option value="Urologue">Urologue</option>
                    <option value="Neurologue">Neurologue</option>
                    <option value="Radiologue">Radiologue</option>
                    <option value="Chiurgien">Chiurgien</option>
                    <option value="Aide-soignant">Aide-soignant</option>
                    <option value="Allergologue">Allergologue</option>
                </select>
                <input className='input' type="date" placeholder="Date" />
                <select className='input'>
                    {times.combinations.map(({ id, timeRange }) => {
                        return <option value={id}>{timeRange}</option>
                    })}
                </select>
                <input className='input' type="text" placeholder="Médecin" />
            </div>
            <div className="PriseRDV-image">
                <p>Choisissez une date</p>
            </div>
            <div className="PriseRDV-valider">
                <p>Choisissez une date</p>
            </div>
        </div>
    );
}

export default PriseRDV;
